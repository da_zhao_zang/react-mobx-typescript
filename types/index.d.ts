/// <reference types="react" />

declare interface SvgComponent extends React.StatelessComponent<React.SVGAttributes<SVGAElement>> {}
declare module '*.svg' {
  const content: SvgComponent
  export default content
}

interface RouteInfo {
  path: string
  children?: Array<RouteInfo>
  exact?: boolean
  redirect?: string
  component?(): Promise<{ default: React.ComponentType }>
}

declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production'
    BUILD_TYPE: 'dev' | 'test' | 'pre' | 'prod'
    API_URL: string
  }
}
