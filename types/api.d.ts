/// <reference types="react" />

interface GoodsParams {
  pageIndex: number
  pageSize: number
}

interface GddosInfo {
  attribute1: number
  attribute2: string
}

interface GoodsList {
  list: Array<GddosInfo>
}
