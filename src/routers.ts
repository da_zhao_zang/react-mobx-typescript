console.log('进入路由')

const Routers: RouteInfo[] = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    exact: true,
    component: () => import(/* webpackChunkName: "home" */ './pages/home'),
  },
  {
    path: '/list',
    exact: true,
    component: () => import(/* webpackChunkName: "list" */ './pages/list'),
  },
  {
    path: '/independent',
    component: () => import(/* webpackChunkName: "independent" */ './pages/independent/test'),
    children: [
      {
        path: '/independent',
        redirect: '/independent/one',
      },
      {
        path: '/independent/one',
        component: () => import(/* webpackChunkName: "independent-one" */ './pages/independent/testOne'),
      },
    ],
  },
]

export default Routers
