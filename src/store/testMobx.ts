import { observable, action } from 'mobx'

class Store {
  @observable text: string = 'hello'
  @observable list: number[] = []

  @action
  setText(text: string) {
    this.text = text
  }

  @action
  setList() {
    let id: number = this.list.length
    this.list.push(id)
  }
}

export const store = new Store()
