/* 引入全局配置 */

import '@babel/polyfill'
import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import Loading from './components/Loading'

ReactDOM.render(
  <Suspense fallback={<Loading />}>
    <App />
  </Suspense>,
  document.getElementById('root')
)
