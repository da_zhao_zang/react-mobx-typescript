/* layout 项目排版 */

import React, { lazy } from 'react'
import routes from './routers'
import Header from './components/Header'
import Footer from './components/Footer'
const RouteView = lazy(() => import('./components/RouteView'))

class App extends React.Component {
  render(): React.ReactNode {
    return (
      <React.Fragment>
        {/* 隐藏公共排版 */}
        {location.pathname.startsWith('/independent') ? (
          <RouteView routes={routes} />
        ) : (
          <React.Fragment>
            <Header />
            <RouteView routes={routes} />
            <Footer />
          </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default App
