import React, { Suspense } from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import AsyncComponent from './AsyncComponent'
import NotFound from '../pages/notFound'

interface RouteViewProps {
  routes: Array<RouteInfo>
}

export default class RouteView extends React.Component<RouteViewProps> {
  constructor(props: Readonly<RouteViewProps>) {
    super(props)
  }

  render() {
    return (
      <Router>
        <Switch>
          {this.props.routes.map((route, key) =>
            route.redirect ? <Route key={key} exact path={route.path} render={() => <Redirect to={route.redirect!} />} /> : <Route key={key} path={route.path} exact={route.exact} render={(props) => <AsyncComponent {...props} component={route.component!} routes={route.children} />} />
          )}
          <Route component={NotFound} />
        </Switch>
      </Router>
    )
  }
}
