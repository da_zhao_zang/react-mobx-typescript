import * as React from 'react'

export default class Loading extends React.Component {
  render() {
    return (
      <div>
        <p>正在加载资源....</p>
      </div>
    )
  }
}
