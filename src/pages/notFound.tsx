import * as React from 'react'

export default class NotFound extends React.Component {
  componentDidMount() {
    var root = document.getElementById('root')
    if (root) {
      root.style.height = '100%'
    }
  }

  render() {
    return (
      <div id='NotFound404'>
        <React.Fragment>
          <h1>404</h1>
          <h2>抱歉,您访问的页面不存在</h2>
        </React.Fragment>
      </div>
    )
  }
}
