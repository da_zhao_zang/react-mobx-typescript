import * as React from 'react'
import { hot } from 'react-hot-loader'
import { Button } from 'antd'

import { store } from '@store/testMobx'
import { observer } from 'mobx-react'

@observer
class List extends React.Component {
  setText(text: string): void {
    store.setText(text)
  }
  setList(): void {
    store.setList()
  }
  render() {
    return (
      <div id='list'>
        <div>{store.text}</div>
        <Button type='primary' onClick={this.setText.bind(this, 'hello mobx')}>
          修改test
        </Button>

        <br />

        <Button type='primary' onClick={this.setList.bind(this)}>
          添加list
        </Button>
        <ul>
          {store.list.map((item) => {
            return <li key={item}>{item}</li>
          })}
        </ul>
      </div>
    )
  }
}

export default hot(module)(List)
