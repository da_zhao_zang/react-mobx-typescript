import * as React from 'react'
import RouteView from '@components/RouteView'
import { RouterProps } from 'react-router'

interface TestProps extends RouterProps {
  routes: Array<RouteInfo>
}

class Test extends React.Component<TestProps> {
  constructor(props: Readonly<TestProps>) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div id='Test'>
        <h2>test</h2>
        <RouteView routes={this.props.routes} />
      </div>
    )
  }
}

export default Test
