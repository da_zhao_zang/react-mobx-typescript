import * as React from 'react'
import { hot } from 'react-hot-loader'
import '../assets/style/common.scss'
import { Button } from 'antd'
import IconExample from '@assets/icons/example.svg'
import { getGoodsList } from '@api/index'

class Home extends React.Component {
  async componentDidMount() {
    let params = {
      pageIndex: 1,
      pageSize: 10,
    }
    const { data } = await getGoodsList(params)
    console.log('data', data.result.list)
  }
  render() {
    return (
      <div id='Home'>
        <h2 className='red'>home</h2>
        <Button type='primary' size='middle'>
          按钮1111111
        </Button>
        <IconExample width={50} height={50} color={'blue'} />
      </div>
    )
  }
}

export default hot(module)(Home)
