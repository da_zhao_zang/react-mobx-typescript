const Servers = {
  // 开发环境接口地址 || 测试环境接口地址
  dev: {
    login: 'login.api-test.com',
    user: 'user.api-test.com',
  },
  // 准生产环境接口地址
  pre: {
    login: 'login.api-pre.com',
    user: 'user.api-pre.com',
  },
  // 生产环境接口地址
  prod: {
    login: 'login.api.com',
    user: 'user.api.com',
  },
}

export const baseApi = Servers[process.env.BUILD_TYPE]
