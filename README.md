## ReactMobxTypescript [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

react-mobx-typescript

### 需要
安装`node.js`  



### 运行  

1. 运行`npm install` 安装所需模块
2. 运行`npm run dev` 启动服务



### 发布

- 测试 `npm run test` 构建测试代码
- 准生产 `npm run pre` 构建准生产代码
- 生产 `npm run prod` 构建生产代码



### 关于发布代码上线：

1. 修改package.json文件中的 version 原则为末尾累加
2. 修改 manifest.Json文件中的 version 保持与package.json 中 version一致
3. 执行package.json 文件中 prod 命令



### 版本号递增原则  

- 单一功能升级或紧急修复 => 末尾版本号加一
- 复杂内容更新或者API不兼容升级调整 => 中部版本号加一
- 架构上的不兼容调整升级或不可降级调整升级 => 首位版本号加一



### 目录说明

- `src` 目录存放前端代码
- `src/api` 目录存放的是api接口
- `src/assets` 目录存放的是项目资源
- `src/components` 目录存放的是组件代码
- `src/http` 目录存放的是封装的http请求
- `src/pages` 目录存放的是页面代码
- `src/store` 目录存放的是状态管理代码
- `src/utils` 目录存放的是通用工具
- `types` 项目Typescript 类型声明
- `doc` 存放项目架构图



### 关于接口地址API_URL说明：

两种方式都可实现，灵活运用

1. 在打包时有变量`process.env.API_URL`，这个参数只能定义string类型，可以在后续api地址中采用拼接方式使用。如：

   ```
   http://user.xxxxx${'-' + process.env.API_URL}.com/xxxx
   http://list.xxxxx${'-' + process.env.API_URL}.com/xxxx
   ```

2. 如后端接口域名不统一，则可以使用`src/api`目录下的`api_env.ts`添加不同域名。如：

   ```
   // 开发环境接口地址 || 测试环境接口地址
   dev: {
     login: 'login.api-test.com',
     user: 'user.api-test.com',
   },
   // 准生产环境接口地址
   pre: {
     login: 'login.api-pre.com',
     user: 'user.api-pre.com',
   },
   // 生产环境接口地址
   prod: {
     login: 'login.api.com',
     user: 'user.api.com',
   },
   ```



### 关于git的使用

项目一般有个三个固定的远程分支develop(用于测试线)/release(用于预生产线)/master(用于正式线) 



### 关于图标的使用
项目中建议优先使用矢量图标, 如果是多彩且无需大小变换的是可直接使用一般位图图标.拿到svg文件注意一下变量 需设置`width`和`height`为`1em` 单色图标需要移除其内部颜色 设置`fill`为`currentColor`



### TSX文件模板
```typescript
import React from 'react'
import { hot } from 'react-hot-loader'

interface ${NAME}Props{}

class ${NAME} extends React.Component<${NAME}Props> {
  render () {
    return (<div id='${NAME}'>

    </div>)
  }
}
export default hot(module)(${NAME})
```



### SVG 文件模板

```
<svg viewBox="0 0 1024 1024" width="1em" height="1em" fill="currentColor" version="1.1" xmlns="http://www.w3.org/2000/svg">
   
</svg>
```



### Mobx引入模板

```
import { NAME } from '@store/NAME'
import { observer } from 'mobx-react'

@observer
```

