module.exports = [
  {
    test: /\.(png|jpe?g|gif)(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 10000,
      name: `imgs/[${process.env.BUILD_TYPE.includes('dev') ? 'name' : 'hash:8'}].[ext]`,
    },
  },
  {
    test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 10000,
      name: `fonts/[${process.env.BUILD_TYPE.includes('dev') ? 'name' : 'hash:8'}].[ext]`,
    },
  },
  {
    test: /\.svg$/,
    use: ['@svgr/webpack'],
  },
]
