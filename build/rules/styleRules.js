const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = [
  {
    test: /\.css|scss$/,
    use: [process.env.BUILD_TYPE.includes('dev') ? 'style-loader' : MiniCssExtractPlugin.loader, 'css-loader?minimize=true', 'sass-loader'],
  },
]
