module.exports = [
  {
    test: /\.tsx?$/,
    exclude: /node_modules/,
    use: [
      'thread-loader',
      {
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          babelrc: false,
          presets: ['@babel/preset-env', '@babel/preset-typescript', '@babel/preset-react'],
          plugins: [
            ['@babel/plugin-proposal-decorators', { legacy: true }],
            ['@babel/plugin-proposal-class-properties', { loose: true }],
            '@babel/plugin-syntax-dynamic-import',
            'react-hot-loader/babel',
            /*  '@babel/plugin-transform-runtime', */
            ['import', { libraryName: 'antd', libraryDirectory: 'es', style: 'css' }, 'ant'],
          ],
        },
      },
    ],
  },
]
