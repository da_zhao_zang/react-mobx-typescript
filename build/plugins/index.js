const webpack = require('webpack')

const pkg = require('./../../package.json')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const Servers = require('./../utils/api_env')

const Server = Servers[process.env.BUILD_TYPE]

if (!Server) {
  throw new Error('请求服务器地址配置错误!')
}

module.exports = [
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    'process.env.BUILD_TYPE': JSON.stringify(process.env.BUILD_TYPE),
    'process.env.API_URL': JSON.stringify(Server),
  }),
  new HtmlWebpackPlugin({
    // favicon: resolve('src/assets/images/Logo.png'),
    filename: 'index.html',
    template: './src/index.html',
    meta: { version: pkg.version },
    minify: {
      removeRedundantAttributes: true, // 删除多余的属性
      collapseWhitespace: true, // 折叠空白区域
      removeAttributeQuotes: true, // 移除属性的引号
      removeComments: true, // 移除注释
      collapseBooleanAttributes: true, // 省略只有 boolean 值的属性值 例如：readonly checked
      minifyCSS: true,
      minifyJS: true,
      html5: true,
    },
  }),
  new webpack.NoEmitOnErrorsPlugin(),
  new MiniCssExtractPlugin({ filename: `css/[name].[${!process.env.BUILD_TYPE.includes('dev') ? 'content' : ''}hash:8].css` }),
  new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /zh-cn/), // moment 组件只加载中文
  new CopyWebpackPlugin([{ context: 'src', from: 'manifest.json', to: 'manifest.json' }]),
]
