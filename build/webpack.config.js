/**
 * @type {import('webpack').Configuration}
 */

const webpack = require('webpack')
const { resolve } = require('./utils/index')

const jsRules = require('./rules/jsRules')
const styleRules = require('./rules/styleRules')
const fileRules = require('./rules/fileRules')

const plugins = require('./plugins/index')

const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

// 根据构建模式设置环境变量
process.env.NODE_ENV = process.env.NODE_ENV = process.env.BUILD_TYPE.startsWith('dev') ? 'development' : 'production'

// 判断生产环境
const IsProduction = process.env.NODE_ENV === 'production'

let webpackConfig = {
  devtool: 'cheap-module-eval-source-map',
  target: 'web',
  mode: process.env.NODE_ENV,
  devServer: {
    port: 9527,
    historyApiFallback: true,
    hot: true,
    https: false,
    progress: true,
    disableHostCheck: true,
    host: '127.0.0.1',
    stats: {
      assets: false,
      children: false,
    },
  },
  cache: true,
  entry: {
    app: [resolve('src/index.tsx')],
  },
  output: {
    publicPath: '/',
    filename: `js/[name].[${!process.env.BUILD_TYPE.includes('dev') ? 'content' : ''}hash:8].js`,
    // chunkFilename: `js/[name].[${!process.env.BUILD_TYPE.includes('dev') ? 'content' : ''}hash:8].js`,
    path: resolve('dist'),
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      automaticNameDelimiter: '.',
      // maxSize: IsProduction ? 200 * 1024 : undefined,
      cacheGroups: {
        // 较大插件可以单独打包 方式如下
        // axios: {
        //   name: 'vendor.axios',
        //   test: /[\\/]node_modules[\\/](moment｜axios)[\\/]/,
        //   priority: -7,
        // },
        // antd: {
        //   name: 'vendor.antd',
        //   test: /[\\/]node_modules[\\/](antd)[\\/]/,
        //   priority: -8, // 数字越大优先级越高 antd依赖moment，所以moment优先级比antd高
        // },
        win: {
          // 插件模块
          test: /[\\/]node_modules[\\/](react-cropper|number-flip|rc-banner-anim|braft-\S+|@ant-design|antd|cropperjs|draft-\S+)[\\/]/,
          name: 'vendor.win',
          priority: -9,
        },
        main: {
          // 公共模块
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor.main',
          priority: -10,
        },
      },
    },
    runtimeChunk: {
      name: 'runtime',
    },
  },
  module: {
    rules: [...jsRules, ...styleRules, ...fileRules],
  },
  plugins: [...plugins],
  resolve: {
    alias: {
      '@': resolve('src'),
      '@api': resolve('src/api'),
      '@assets': resolve('src/assets'),
      '@components': resolve('src/components'),
      '@store': resolve('src/store'),
      '@utils': resolve('src/utils'),
    },
    extensions: ['.js', '.json', '.ts', '.tsx'],
  },
  // 忽略一些常用包的集成，从而降低构建时间和打包后的包大小 忽略后需要在index.html加载cdn资源
  externals: {
    // react: 'window.React',
  },
}

/**
 * Adjust webpackConfig for production settings
 */
if (IsProduction) {
  webpackConfig.devtool = ''
  webpackConfig.plugins.push(
    new ForkTsCheckerWebpackPlugin(),
    new webpack.HashedModuleIdsPlugin()
    // new webpack.optimize.MinChunkSizePlugin({minChunkSize: 100 * 1024})
  )
} else {
  if (process.env.BUILD_TYPE.includes('dev')) {
    webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin(), new webpack.NamedModulesPlugin())
  }
}

module.exports = webpackConfig
