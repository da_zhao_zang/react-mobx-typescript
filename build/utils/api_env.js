module.exports = {
  // 开发环境接口地址
  dev: 'test',
  // 测试环境接口地址
  test: 'test',
  // 准生产环境接口地址
  pre: 'pre',
  // 生产环境接口地址
  prod: 'prod',
}
